const Mongoose = require('mongoose');

const projects = new Mongoose.Schema({
  _id: Mongoose.Types.ObjectId,
  title: {
    type: String,
    unique: true,
    minlength: 3,
    maxlength: 20,
  },
  description: {
    type: String,
    required: true,
    minlength: 2,
    maxlength: 255,
  },
  date: Date,
});
module.exports = Mongoose.model('Projects', projects);
