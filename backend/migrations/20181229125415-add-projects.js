// db-migrate up --config config.json -e "db"
// db-migrate create add-projects --config config.json -e "db"
'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  db.createCollection('projects', (err) => {
    if (err) throw err;
    console.log('Successfully create collection projects');
  });
};

exports.down = function(db) {
  db.dropCollection('projects',  (err) => {
    if (err) throw err;
     console.log('Successfully drop collection projects');
  });
};

exports._meta = {
  "version": 1
};
