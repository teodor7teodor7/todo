const User = require('../models/user');
var Mongoose = require('mongoose');
var bCrypt = require('bcrypt-nodejs');

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};
exports.up = function(db) {
  db.createCollection('users', (err) => {
    if (err) throw err;
     console.log('Successfully create collection users');

     db.insert('users', new User({
         _id: Mongoose.Types.ObjectId,
         username: 'admin',
         password: bCrypt.hashSync('123456', bCrypt.genSaltSync(10), null),
         email: 'admin@default.deafault',
         date: new Date()
       }), (err) => {
         if (err) throw err;
          console.log('Successfully add admin in collection users');
     });

  })
};

exports.down = function(db) {
  db.dropCollection('users',  (err) => {
    if (err) throw err;
     console.log('Successfully drop collection user');
  });
};

exports._meta = {
  "version": 1
};
