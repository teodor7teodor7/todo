var express = require('express');
const isAuthenticated = require('../middleware/isAuthenticated');

var router = express.Router();

/* GET home page. */
router.get('/', isAuthenticated, function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
