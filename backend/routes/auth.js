const express = require('express');
const jwt = require('jsonwebtoken');
const config = require('../config.json');
const User = require('../models/user');
const bCrypt = require('bcrypt-nodejs');

const ACCESS_TIME = Math.floor(new Date().getTime() / 1000) + 5 * 60;
const REFRESH_TIME = Math.floor(new Date().getTime() / 1000) + 7 * 60 * 60;
const TIME_NOW = Math.floor(new Date().getTime() / 1000);

const router = express.Router();

module.exports = (passport) => {
  router.post('/login', (req, res) => {
    passport.authenticate('local', { session: false }, (err, user, message) => {
      if (err || !user) {
        return res.status(401).json({ message });
      }
      const accessToken = jwt.sign({
        auth: 'todo',
        agent: req.headers['user-agent'],
        exp: ACCESS_TIME,
        user: { _id: user._id, email: user.email }
      },
      config.secretOrKey);
      const refreshToken = jwt.sign({
        auth: 'todo',
        agent: req.headers['user-agent'],
        exp: REFRESH_TIME,
        user: { _id: user._id, email: user.email }
      },
      bCrypt.hashSync(Math.floor(new Date().getTime()), bCrypt.genSaltSync(10), null));
      User.findById(user._id, (error, user) => {
        user.set({ refreshToken });
        user.save((err) => {
          if (err) { return res.status(401).json({ message: 'Something is not right' }); }
        });
      });

      return res.json({ accessToken, refreshToken });
    })(req, res);
  });

  router.get('/logout', (req, res) => {
    req.logout();
    res.send({});
  });

  router.post('/refresh', (req, res) => {
    const { headers: { authorization } } = req;
    const rawToken = authorization.split(' ')[1];
    let accessToken, refreshToken;
    let decode = jwt.decode(rawToken);
    if(decode.exp < TIME_NOW)  { return res.status(401).json({ message: 'Something is not right' }); }
    User.findOne({ refreshToken: rawToken }, (err, user) => {
        if (err || !!!user) { return res.status(401).json({ message: 'Something is not right' }); }
        accessToken = jwt.sign({
          auth: 'todo',
          agent: req.headers['user-agent'],
          exp: ACCESS_TIME,
          user: { _id: user._id, email: user.email }
        },
        config.secretOrKey);
        refreshToken = jwt.sign({
          auth: 'todo',
          agent: req.headers['user-agent'],
          exp: REFRESH_TIME,
          user: { _id: user._id, email: user.email }
        },
        bCrypt.hashSync(Math.floor(new Date().getTime()), bCrypt.genSaltSync(10), null));

        User.findOneAndUpdate({ refreshToken: rawToken }, { refreshToken }, { upsert: true }, function(err, doc){
            if (err) { return res.status(401).json({ message: 'Something is not right' }); }
        });
      return res.status(200).json({ accessToken,  refreshToken });
    });

  });
  return router;
};
