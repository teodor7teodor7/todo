const newsController = require('../controllers/newsController');

module.exports = (io) => {
  const live = io.of('/live');
  live.on('connect',
    async (socket) => {
      console.log('User has connected to Index');
      socket.on('channel', async (channel) => {
        console.log(channel);
        socket.join(channel);
        live.in(channel).emit('news', await newsController.wsClient(channel));
      });
      socket.on('leave', async () => {
        console.log('leave');
        socket.leave('undefined');
      });
    });
};
