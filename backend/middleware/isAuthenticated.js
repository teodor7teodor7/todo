const jwt = require('jsonwebtoken');
const User = require('../models/user');
const config = require('../config.json');

module.exports = async (req, res, next) => {
  const { headers: { authorization } } = req;
  try {
    const rawToken = authorization.split(' ')[1];
    const accessToken = jwt.verify(rawToken, config.secretOrKey);
    const user = await User.findOne({ _id: accessToken.user._id })
    if (user) {
      req.user = accessToken.user; return next();
    } return res.status(401).json({ status: '401' });
  } catch (err) {
    return res.status(401).json({ status: '401' });
  }
};
